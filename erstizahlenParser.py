# script for parsing freshman matriculation numbers

## import packages
import os
import requests
import json
from datetime import datetime

TypeOfAbschluss = ['Bachelor (2-Fächer/Profil Lehramt)', 'Bachelor (2 Fächer)', 'Bachelor', 'Master'] 
null_date =  {
            "info_bsc": [0,0],
            "info_msc": [0,0],
            "info_2fa": [0,0],
            "info_2fal": [0,0],
            "math_bsc": [0,0],
            "math_msc": [0,0],
            "math_2fa": [0,0],
            "math_2fal": [0,0],
            "ads_bsc": [0,0],
            "mds_bsc": [0,0],
            "ads_msc": [0,0]
}

def requestFlexStat(studfach,studfach_bez):
    session = requests.Session()
    response = session.get('https://pruefungsverwaltung.uni-goettingen.de/statistikportal#parameters?query=103')
    request_data = {'data': '{"id":103,"title":"Immatrikulierte pro Studiengang (aktuell) - Fachf\u00e4lle","description":"\r\n\tImmatrikulierte pro Studiengang mit Angabe der Kohorte. Optional kann zwischen Deutschland und nicht Deutschland als Herkunftsland unterschieden werden. \r\n\tEnth\u00e4lt die alte Abfrage 195.\r\n\t","outputcolumns":"Studiengang | Abschluss | Kohorte | Immatrikuliert (ohne Beurl.) | Beurlaubt | Gesamtanzahl | M\u00e4nnlich | Weiblich | M\u00e4nnlich, Deutsche | M\u00e4nnlich, nicht Deutsche | Weiblich, Deutsche | Weiblich, nicht Deutsche ","parameters":[{"name":"Studiengang","type":0,"field":"STUDFACH","identifier":"STUDFACH_1_count1","optional":false,"order_count_identifier":"1_count1","associatedFields":[{"name":"Studiengang","type":0,"lastValue":"'+studfach+'","lastDisplayValue":"'+studfach_bez+'","selectAllDummy":false,"identifier":"STUDFACH_1_count1","field":"studfach","optional":false}]},{"name":"Anzeige Herkunftsland Deutschland","type":1,"field":"CHECKBOX","identifier":"CHECKBOX_2_count1","optional":true,"order_count_identifier":"2_count1","associatedFields":[{"name":"Anzeige Herkunftsland Deutschland","type":1,"lastValue":"false","lastDisplayValue":"Nein","selectAllDummy":false,"identifier":"CHECKBOX_2_count1","field":"CheckBox","optional":true}]}]}'}
    response = session.post('https://pruefungsverwaltung.uni-goettingen.de/statistikportal/api/queryexecution/results', data = request_data)
    return json.loads(response.text)

def printMeta(data_raw):
    print("Request successful: ", data_raw['success'])
    print("Main keys of request responst: ", data_raw['data'].keys())
    metaData = data_raw['data']['metaData']
    print("queryName: ", metaData['queryName'])
    print("queryID:", metaData['id'])
    print("executionTimestamp: ", metaData['executionTimestamp'])
    columns = [x['name'] for x in data['data']['metaData']['fields']]
    print("Column names: ", columns)

def updateJson(filename, date, course, count):
    # update json data-file
    with open(filename, 'r+') as f:
        data = json.load(f)
        data['data'][date][course][0] += count[0]
        data['data'][date][course][1] += count[1]
        f.seek(0)
        print(data)
        json.dump(data,f, indent=4)

    return 0


def extractData(data_raw, course, new_date, date):
    success = data_raw['success']
    timestamp = data_raw['data']['metaData']['executionTimestamp']
    dt_obj = datetime.fromtimestamp(timestamp)
    if success:
        data = data_raw['data']['records']
        n = data_raw['data']['total'] # total number of entries/rows
        count_m = 0
        count_f = 0
        print("Total number of entries in dataset: ", n)
        for row in data:
            #cohort = row['Kohorte']
            isCurrentcohort = (row['Kohorte'] == "WS{}/{}".format(year%100,(year%100)+1))
            isAbschluss = row['Abschluss'] in TypeOfAbschluss
            if isCurrentcohort:
                if isAbschluss:
                    count_m = row['Männlich']
                    count_f = row['Weiblich']
                    #count_b = row['Beurlaubt']
                    #count_imm = row['Immatrikuliert (ohne Beurl_)'] # immatrikuliert ohne Beurlaubung
                    #count_sum = row['Gesamtanzahl']
                    # update json data-file
                    if "Lehramt" in row['Abschluss']:
                        print("{} (L)".format([row['Männlich'],row['Weiblich']]))
                        new_date[course+"l"][0] += count_m
                        new_date[course+"l"][1] += count_f
                    else:
                        print([row['Männlich'],row['Weiblich']])
                        new_date[course][0] += count_m
                        new_date[course][1] += count_f
                if row['Abschluss'] == 'Master bedingte Einschreibung':
                    count_m = row['Männlich']
                    count_f = row['Weiblich']
                    print("{} (Bed)".format([row['Männlich'],row['Weiblich']]))
                    new_date[course][0] += count_m
                    new_date[course][1] += count_f
                
    return

def visualizeData(year):
    print("Visualization WIP")
    return

if __name__ == "__main__":
    verbose = False
    update = True

    year = 2022
    filename = "erstizahlen/erstizahlen{}.json".format(year%100)
    courses = ['info_bsc','math_bsc', 'info_2fa', 'math_2fa', 'ads_bsc', 'mds_bsc', 'info_msc', 'math_msc', 'ads_msc']
    studfach = ['BSI', "BSMAT", 'BAINF', 'BAMAL', 'BSADS', 'BSMDS', 'MAAI', 'MSMAT', 'MSADS']
    studfach_bez = ["Angewandte Informatik (Bachelor of Science)",
            "Mathematik (Bachelor of Science)", 
            "Informatik (2-F\u00e4cher-Bachelor)",
            "Mathematik (2-F\u00e4cher-Bachelor)",
            "Angewandte Data Science (Bachelor of Science", 
            "Mathematical Data Science (Bachelor of Science)",
            "Angewandte Informatik (Master of Science)",
            "Mathematik (Master of Science)",
            "Angewandte Data Science (Master of Science)"
    ]

    if not os.path.exists(filename):
        print("creating file")
        with open(filename, 'w') as f:
            s = {
                "name": "erstizahlen",
                "id": "db-erstizahlen-{}".format(year%100),
                "year": year,
                "data": {}
            }
            json.dump(s, f, indent=4)

    date = datetime.now().strftime('%Y%m%d')
    print("Date: ", date)
    new_date = null_date.copy()

    for i in range(len(studfach)):
        print("Request on: {} ({})".format(studfach_bez[i], studfach[i]))

        if update:
            data = requestFlexStat(studfach[i], studfach_bez[i])
        else: 
            #read from file
            pass
        if verbose:
            printMeta(data)
        extractData(data, courses[i], new_date, date) #add to json/database
        #visualizeData(year)

    #update json
    with open(filename, 'r+') as f:
        json_data = json.load(f)
        if not date in json_data['data']:
            json_data['data'][date] = {}
        json_data['data'][date] = new_date
        f.seek(0)
        json.dump(json_data, f, indent=4)
