#!/usr/bin/env python3
import sqlite3
from sqlite3 import Error
import os
import json
import re
from datetime import datetime

def connectDB(dbfile):
    conn = None
    try:
        conn = sqlite3.connect(dbfile)
    except Error as e:
        print(e)

    return conn

def insertRow(conn, row):
    sql = ''' INSERT INTO freshmanNumbers(date, year, 
        info_bsc_gesamt, info_bsc_m, info_bsc_f, 
        info_msc_gesamt, info_msc_m, info_msc_f, 
        info_2fa_gesamt, info_2fa_m, info_2fa_f, 
        info_2fal_gesamt, info_2fal_m, info_2fal_f, 
        math_bsc_gesamt, math_bsc_m, math_bsc_f, 
        math_msc_gesamt, math_msc_m, math_msc_f, 
        math_2fa_gesamt, math_2fa_m, math_2fa_f, 
        math_2fal_gesamt, math_2fal_m, math_2fal_f, 
        ads_bsc_gesamt, ads_bsc_m, ads_bsc_f, 
        mds_bsc_gesamt, mds_bsc_m, mds_bsc_f, 
        ads_msc_gesamt, ads_msc_m, ads_msc_f) 
        VALUES(?,?, ?,?,?, ?,?,?, ?,?,?, ?,?,?, ?,?,?, ?,?,?, ?,?,?, ?,?,?, ?,?,?, ?,?,?, ?,?,?)
    '''
    cur = conn.cursor()
    cur.execute(sql, row)
    conn.commit()
    return cur.lastrowid #rowid


def migrate(conn):
    filelist = os.listdir('erstizahlen/')
    filelist = [f for f in filelist if re.match(r"erstizahlen\d{2}", f)]

    for file in filelist:
        data = None
        with open('erstizahlen/' + file, 'r') as jsonfile:
            data = json.load(jsonfile)
        
        print(data["year"])
        for key in data["data"]:
            values = data["data"][key]
            date = datetime.strptime(key, '%Y%m%d')
            row = (date, data["year"],
                values["info_bsc"][0] + values["info_bsc"][1], values["info_bsc"][0], values["info_bsc"][1],
                values["info_msc"][0] + values["info_msc"][1], values["info_msc"][0], values["info_msc"][1],
                values["info_2fa"][0] + values["info_2fa"][1], values["info_2fa"][0], values["info_2fa"][1],
                values["info_2fal"][0] + values["info_2fal"][1], values["info_2fal"][0], values["info_2fal"][1],
                values["math_bsc"][0] + values["math_bsc"][1], values["math_bsc"][0], values["math_bsc"][1],
                values["math_msc"][0] + values["math_msc"][1], values["math_msc"][0], values["math_msc"][1],
                values["math_2fa"][0] + values["math_2fa"][1], values["math_2fa"][0], values["math_2fa"][1],
                values["math_2fal"][0] + values["math_2fal"][1], values["math_2fal"][0], values["math_2fal"][1]
                )
            if "ads_bsc" in values.keys():
                row += (
                    values["ads_bsc"][0] + values["ads_bsc"][1], values["ads_bsc"][0], values["ads_bsc"][1],
                    values["mds_bsc"][0] + values["mds_bsc"][1], values["mds_bsc"][0], values["mds_bsc"][1]
                )
            else: 
                row += (None, None, None, None, None, None)

            if "ads_msc" in values.keys():
                row += (values["ads_msc"][0] + values["ads_msc"][1], values["ads_msc"][0], values["ads_msc"][1])
            else:
                row += (None, None, None)

            print(row)
            insertRow(conn, row)
    


def main(dbfile):
    conn = connectDB(dbfile)
    cur = conn.cursor()

    #create data table
    cur.execute('''
        CREATE TABLE IF NOT EXISTS freshmanNumbers(
        date TEXT PRIMARY KEY,
        year INTEGER NOT NULL,
        info_bsc_gesamt INTEGER NOT NULL DEFAULT 0,
        info_bsc_m INTEGER NOT NULL DEFAULT 0,
        info_bsc_f INTEGER NOT NULL DEFAULT 0,
        info_msc_gesamt INTEGER NOT NULL DEFAULT 0,
        info_msc_m INTEGER NOT NULL DEFAULT 0,
        info_msc_f INTEGER NOT NULL DEFAULT 0,
        info_2fa_gesamt INTEGER NOT NULL DEFAULT 0,
        info_2fa_m INTEGER NOT NULL DEFAULT 0,
        info_2fa_f INTEGER NOT NULL DEFAULT 0,
        info_2fal_gesamt INTEGER NOT NULL DEFAULT 0,
        info_2fal_m INTEGER NOT NULL DEFAULT 0,
        info_2fal_f INTEGER NOT NULL DEFAULT 0,
        math_bsc_gesamt INTEGER NOT NULL DEFAULT 0,
        math_bsc_m INTEGER NOT NULL DEFAULT 0,
        math_bsc_f INTEGER NOT NULL DEFAULT 0,
        math_msc_gesamt INTEGER NOT NULL DEFAULT 0,
        math_msc_m INTEGER NOT NULL DEFAULT 0,
        math_msc_f INTEGER NOT NULL DEFAULT 0,
        math_2fa_gesamt INTEGER NOT NULL DEFAULT 0,
        math_2fa_m INTEGER NOT NULL DEFAULT 0,
        math_2fa_f INTEGER NOT NULL DEFAULT 0,
        math_2fal_gesamt INTEGER NOT NULL DEFAULT 0,
        math_2fal_m INTEGER NOT NULL DEFAULT 0,
        math_2fal_f INTEGER NOT NULL DEFAULT 0,
        ads_bsc_gesamt INTEGER DEFAULT 0,
        ads_bsc_m INTEGER DEFAULT 0,
        ads_bsc_f INTEGER DEFAULT 0,
        mds_bsc_gesamt INTEGER DEFAULT 0,
        mds_bsc_m INTEGER DEFAULT 0,
        mds_bsc_f INTEGER DEFAULT 0,
        ads_msc_gesamt INTEGER DEFAULT 0,
        ads_msc_m INTEGER DEFAULT 0,
        ads_msc_f INTEGER DEFAULT 0
        ) '''
        )

    status = migrate(conn)
    
    cur = conn.cursor()
    cur.execute('SELECT * from freshmanNumbers')
    data = cur.fetchall()
    for row in data:
        print(row)
        
    conn.close()


if __name__ == "__main__":
    dbfile = "data.db"
    main(dbfile)
